#!/usr/bin/env python3
'''
    fg.py is a Flask wrapper for kanali_feed.py
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


from flask import Flask, Response, render_template
from kanali_feed import feedgen as k6

app = Flask(__name__)


@app.route('/k6.rss')
def generate_rss():
    feed = k6()
    rss = feed.rss_str(pretty=True)
    return Response(rss, mimetype='text/xml')


@app.route('/')
def index():
    return render_template("index.html", msg="RSS feed for Kanali 6 podcasts.\
                           AGPLv3")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
