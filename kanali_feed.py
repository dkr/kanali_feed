#!/usr/bin/env python3
'''
    kanali_feed.py generates an RSS feed for Kanali 6 podcasts
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from  urllib.request import Request, urlopen
import xmltodict
from feedgen.feed import FeedGenerator
from datetime import datetime, timedelta
from pytz import timezone
import locale

# to get properly formatted time stamps
locale.setlocale(locale.LC_TIME, "el_CY")

def feedgen():
    # Prepare Request with Kanali 6 URL and acceptable user-agent
    req = Request('https://kanali6.com.cy/mp3/TOC.xml',
                  headers = {'User-Agent': 'Mozilla/5.0'})

    # Fetch XML file, decode as utf-8
    with urlopen(req) as response:
       xml = response.read().decode('utf-8')

    # XML to dict
    toc = xmltodict.parse(xml, process_namespaces= True)

    # Set up feed generation, obligatory tags
    fg = FeedGenerator()
    fg.title('Κανάλι Έξι')
    fg.author({'name':'Κανάλι Έξι', 'email':'info@kanali6.com.cy'})
    fg.description('Για τον Άνθρωπο και τον Πολιτισμό')
    fg.link( href='https://kanali6.com.cy', rel='alternate' )
    fg.logo('https://kanali6.com.cy/wp-content/uploads/2019/11/kanali6_logo_dark.png')
    fg.language('el')
    # Add new RSS entry loop
    url_prefix = 'https://kanali6.com.cy/mp3/' # in case we port to other radio

    # we don't want to bother with recordings older than 7 days at runtime
    maxage = timezone('Asia/Nicosia').localize(
            datetime.now()) - timedelta(days=7)

    for rec in toc['podcasts']['recording']:
        # make timestamps timezone aware / dtc = datetime corrected
        date_str = rec['RecStartedDateTime']  # 2020-03-28T15:10:00
        dt = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S")  # parse dt
        dtc = timezone('Asia/Nicosia').localize(dt)  #2020-03-28 15:00:00+02:00
#        print(rec['Filename'] + ": " + rec['Title'] + ", " +
#          rec['RecStartedDateTime'])  # comment out in production
#        print((dtc - maxage).days)
        if (dtc - maxage).days >= 0:
            print("+ " + rec['Filename'] + ": " + rec['Title'].title() +
              " - " + dtc.strftime("%x"))
            uri = url_prefix + rec['Filename']
            fe = fg.add_entry()
            #fe.id(rec['RecordingID'])  # should be unique
            fe.id(uri)
            fe.title(rec['Title'].title() + " - " + dtc.strftime("%x"))
            fe.pubDate(dtc)
            try:
                fe.description("Εκπομπή " + rec['Title'].title() + ", με "
                               + rec['ShowInformation']['ProducersName'].
                               replace(",", ", ").title()
                               + " στις " + dtc.strftime("%x (%A) - %H:%M %Z"))
            except AttributeError:  # when recording doesn't have producers
                fe.description("Εκπομπή " + rec['Title'].title()
                               + " στις " + dtc.strftime("%x (%A) - %H:%M %Z"))
            fe.enclosure(uri, 0, 'audio/mpeg')
#            fg.rss_file('podcast.xml', pretty=True)
        else:
            pass
    return(fg)

if __name__ == '__main__':
    kanali = feedgen()
    try:
        kanali.rss_file('podcast.xml', pretty=True)
    except PermissionError:
        print("The working directory is not writable. Exiting without saving.")
